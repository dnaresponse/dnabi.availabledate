﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DnaBI.AvailableDate.DAL
{
   public class UidDA
    {
       string connectionstring = "";
       public UidDA()
       {
           connectionstring = System.Configuration.ConfigurationManager.AppSettings["DBConn"]; 
       }
     public List<string> GetUidWithemptyAvailableDate()
       {
           List<string> lstUId = new List<string>();
           try
           {
               using (SqlConnection conn = new SqlConnection(connectionstring))
               {
                   conn.Open();
                   SqlCommand cmd = new SqlCommand("spGetUIDForBalankAvailableDate", conn);
                   cmd.CommandType = CommandType.StoredProcedure;
                   using (SqlDataReader reader = cmd.ExecuteReader())
                   {
                       while (reader.Read())
                       {
                                                   
                            string uid = reader.GetValue(0).ToString();
                            lstUId.Add(uid);

                       }
                   }

               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
          
           return lstUId;
       }

     public void updatefirstavailableDate(string UId,DateTime availableDate)
     {
         try
         {
             using (SqlConnection conn = new SqlConnection(connectionstring))
             {
                 conn.Open();
                 SqlCommand cmd = new SqlCommand("spUpdateFirstAvailableDate", conn);
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.Parameters.AddWithValue("@UID", UId);
                 cmd.Parameters.AddWithValue("@FirstAvailableDate", availableDate);
                 cmd.ExecuteNonQuery();

             }
         }
         catch (Exception ex)
         {
             throw ex;
         }
     }
    }
}
