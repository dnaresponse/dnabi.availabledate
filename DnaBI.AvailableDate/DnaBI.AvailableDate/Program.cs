﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DnaBI.AvailableDate.DAL;
using DnaBI.AvailableDate.AWSAPI;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using DnaBI.AvailableDate.BL;

namespace DnaBI.AvailableDate
{
    class Program
    {
        static void Main(string[] args)
        {
            UidDA ObjUId = new UidDA();
           List<string> lstUiD= ObjUId.GetUidWithemptyAvailableDate();
           CookieContainer cookieJar = new CookieContainer();
           CookieCollection cookies = new CookieCollection();
           foreach (string uid in lstUiD)
           {
               DateTime availableDatetime = new DateTime();
               ItemLookup itemlookup = new ItemLookup();
               string availableDate = itemlookup.AmazonItemLookup(uid);
               if (string.IsNullOrEmpty(availableDate))
               {
                   HttpWebResponse firstResponse = null;
                   string url = "http://www.amazon.com/dp/" + uid;
                   #region WebRequst
                   StringBuilder str = new StringBuilder();
                   HttpWebRequest firstRequest = (HttpWebRequest)WebRequest.Create(url);
                   firstRequest.CookieContainer = cookieJar;
                   firstRequest.AllowAutoRedirect = false;
                   firstRequest.KeepAlive = true;
                   firstRequest.Method = "GET";
                   firstRequest.ContentType = "text/html; charset=UTF-8";
                   firstRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.0; rv:29.0) Gecko/20100101 Firefox/29.0";
                   firstRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                   #endregion
                   firstResponse = (HttpWebResponse)firstRequest.GetResponse();

                   using (StreamReader reader = new StreamReader(firstResponse.GetResponseStream()))
                   {

                       string strXML = reader.ReadToEnd();
                       HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

                       cookies = firstResponse.Cookies;
                       cookieJar.Add(cookies);

                       Match m1 = Regex.Match(strXML, "Date first available at Amazon.com:</b>(.*?)<", RegexOptions.IgnoreCase);
                       if (m1.Success)
                       {
                           if (!string.IsNullOrEmpty(Convert.ToString(m1.Groups[1].Value.Trim())))
                           {
                               availableDate = m1.Groups[1].Value.Trim();
                               availableDatetime = Convert.ToDateTime(m1.Groups[1].Value.Trim());

                           }
                       }
                       if (string.IsNullOrEmpty(availableDate))
                       {

                           AvailableDates ObjDate = new AvailableDates();
                           availableDatetime = ObjDate.GetListingDate(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ReviewURL"]).Replace("##ASIN##", uid));
                       }


                   }

               }
               else
               {
                   availableDatetime = Convert.ToDateTime(availableDate);
               }
               ObjUId.updatefirstavailableDate(uid, availableDatetime);
           }

        }
    }
}
