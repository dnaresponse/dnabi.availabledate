﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Xml;
using System.Net;
using System.Xml.Linq;

namespace DnaBI.AvailableDate.AWSAPI
{
    public class ItemLookup
    {

        public string AmazonItemLookup(string UId)
        {
            string releaseDate = "";

            #region Object            
            Dictionary<String, String> AWSAPIConfig = new Dictionary<string, string>();
            IDictionary<string, string> r1 = new Dictionary<string, String>();
            #endregion

            #region VARIABLES
            string AWS_ACCESS_KEY_ID = System.Configuration.ConfigurationManager.AppSettings["AWS_ACCESS_KEY_ID"];
            string AWS_SECRET_KEY = System.Configuration.ConfigurationManager.AppSettings["AWS_SECRET_KEY"];
            string DESTINATION = System.Configuration.ConfigurationManager.AppSettings["DESTINATION"];
            string ConfigFilePath = System.Environment.CurrentDirectory.ToString() + "\\Configurations\\AWSConfig.json";
            string requestUrl = string.Empty;
            #endregion
            SignedRequestHelper helper = new SignedRequestHelper(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY, DESTINATION);
            //AWS API Config..
            String strAWSAPIConfig = File.ReadAllText(ConfigFilePath);

            AWSAPIConfig = JsonConvert.DeserializeObject<Dictionary<String, String>>(strAWSAPIConfig);

            #region API Request Paramenters
            r1["Service"] = AWSAPIConfig["Service"];
            r1["AssociateTag"] = AWSAPIConfig["AssociateTag"];
            r1["Version"] = AWSAPIConfig["Version"];
            r1["Operation"] = AWSAPIConfig["Operation"];
            r1["ResponseGroup"] = AWSAPIConfig["ResponseItemGroup"];
            r1["SignatureVersion"] = AWSAPIConfig["SignatureVersion"];
            r1["SignatureMethod"] = AWSAPIConfig["SignatureMethod"];
            r1["Timestamp"] = Uri.EscapeDataString(DateTime.UtcNow.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z"));
            r1["Condition"] = AWSAPIConfig["Condition"];
            r1["ItemId"] = UId;
            r1["IdType"] = AWSAPIConfig["IdType"]; ;
            #endregion
                requestUrl = helper.Sign(r1);
                int SearchID = 0;
                XmlDocument xmlDoc = sendRequest(requestUrl, SearchID);

                if (xmlDoc == null)
                {
                    xmlDoc = sendRequest(requestUrl, SearchID);
                }
                if (xmlDoc != null)
                {
                    
                     XDocument doc = XDocument.Load(new XmlNodeReader(xmlDoc));

                    XNamespace ns = AWSAPIConfig["namespace"];
                     if (doc.Element(ns + "ItemLookupResponse").Elements(ns + "Items").Elements(ns + "Request").Elements(ns + "Errors").Count() > 0)
                     {
                     }
                     else
                     {
                        
                       var  brand = (from items in doc.Element(ns + "ItemLookupResponse").Elements(ns + "Items").Elements(ns + "Item")
                          select new
                          {
                              ReleaseDate = items.Elements(ns + "ItemAttributes").Elements(ns + "ReleaseDate").Count() > 0 ? Convert.ToString(items.Elements(ns + "ItemAttributes").Elements(ns + "ReleaseDate").SingleOrDefault().Value) : string.Empty
                          });
                       releaseDate = brand.SingleOrDefault().ReleaseDate;
                         
                     }



                }

                return releaseDate;

        }
        private static XmlDocument sendRequest(string url, int SearchID)
        {          
          
            try
            {
                string NAMESPACE = System.Configuration.ConfigurationManager.AppSettings["NAMESPACE"];

                WebRequest request = HttpWebRequest.Create(url);
                WebResponse response = request.GetResponse();
                XmlDocument doc = new XmlDocument();
                doc.Load(response.GetResponseStream());

                XmlNodeList errorMessageNodes = doc.GetElementsByTagName("Message", NAMESPACE);
                if (errorMessageNodes != null && errorMessageNodes.Count > 0)
                {
                    String message = errorMessageNodes.Item(0).InnerText;
                    throw new Exception("Error " + message);
                }

                return doc;
            }
            catch (Exception ex)
            {

            }         

            return null;
        }// end FetchTitle()

    }
}
