﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using DnaBI.AvailableDate;

namespace DnaBI.AvailableDate.BL
{
    public class AvailableDates
    {
        public DateTime GetListingDate(string Link)
        {
            //var currentMethod = System.Reflection.MethodBase.GetCurrentMethod();
            //var fullMethodName = currentMethod.DeclaringType.FullName + "." + currentMethod.Name;
            //Dna.AuditLogging.AsyncLog Log = new Dna.AuditLogging.AsyncLog();


            string ListingDate = String.Empty;
            int ListingDateType;
            DateTime ListingDateTime = new DateTime();
            CookieContainer cookieJar = new CookieContainer();
            CookieCollection cookies = new CookieCollection();
            try
            {
                //Log.Log("Listings", fullMethodName, Log.BEGIN, null, null, "GetListingDate " + Link, SearchID.ToString(), common.ApplicationName, strConnectionString);

                #region REQUEST
                StringBuilder str = new StringBuilder();
                HttpWebRequest firstRequest = (HttpWebRequest)WebRequest.Create(Link);
                firstRequest.CookieContainer = cookieJar;
                firstRequest.AllowAutoRedirect = false;
                firstRequest.KeepAlive = true;
                firstRequest.Method = "GET";
                firstRequest.ContentType = "text/html; charset=UTF-8";
                firstRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.0; rv:29.0) Gecko/20100101 Firefox/29.0";
                firstRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                HttpWebResponse firstResponse = (HttpWebResponse)firstRequest.GetResponse();
                cookies = firstResponse.Cookies;
                cookieJar.Add(cookies);
                #endregion

                #region RESPONSE
                using (StreamReader reader = new StreamReader(firstResponse.GetResponseStream()))
                {
                    string strXML = reader.ReadToEnd();
                    Match mdate = Regex.Match(strXML, "<nobr>\\s*([a-z]+\\s*\\d+,\\s*\\d{4})\\s*</nobr>", RegexOptions.IgnoreCase);
                    if (mdate.Success)
                    {
                        ListingDate = mdate.Groups[1].Value;
                        ListingDateTime = Convert.ToDateTime(ListingDate);
                        ListingDateType = 2;
                    }
                    else
                    {
                        ListingDateTime = DateTime.UtcNow;
                        ListingDateType = 3;
                    }
                }
                #endregion

               // Log.Log("Listings", fullMethodName, Log.COMPLETE, null, null, "GetListingDate " + Link, SearchID.ToString(), common.ApplicationName, strConnectionString);

                return ListingDateTime;
            }
            catch (WebException ex)
            {
                //string ErrorMessage = "The remote server returned an error: ";
                //if (ex.Status == WebExceptionStatus.ProtocolError || ex.Status == WebExceptionStatus.ConnectFailure || ex.Status == WebExceptionStatus.SendFailure)
                //{
                //    ErrorMessage += "Status Code : " + (int)((HttpWebResponse)ex.Response).StatusCode;
                //    ErrorMessage += " Status Description : " + ((HttpWebResponse)ex.Response).StatusDescription;
                //}
                //else
                //{
                //    ErrorMessage = ex.InnerException.Message;
                //}
                //Log.Log("Listings", fullMethodName, Log.FAIL, null, null, ErrorMessage, SearchID.ToString(), common.ApplicationName, strConnectionString);
                throw;
            }
            catch (Exception ex)
            {
              //  Log.Log("Listings", fullMethodName, Log.FAIL, null, null, ex.Message + ":" + ex.InnerException, SearchID.ToString(), common.ApplicationName, strConnectionString);

                throw;
            }
        }
    }
}
